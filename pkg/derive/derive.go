package derive

import (
	"encoding/hex"
	"fmt"
	"log"

	"github.com/ethereum/go-ethereum/accounts"
	"github.com/ethereum/go-ethereum/accounts/usbwallet"
)

const (
	PathHardenedBase = 0x80000000
	PathMax          = 0x7FFFFFFF
	PathEthPurpose   = PathHardenedBase + 44
	PathEthCoinType  = PathHardenedBase + 60
)

func DerivationPath(account uint32) []uint32 {
	// m’ / purpose’ / coin_type’ / account’ / change / address_index
	// m  / 44'      / 60'        /          / 0      / 0
	return accounts.DerivationPath{PathEthPurpose, PathEthCoinType, account, 0, 0}
}

func HumanDerivationPath(account uint32) string {
	// m’ / purpose’ / coin_type’ / account’ / change / address_index
	// m  / 44'      / 60'        /          / 0      / 0
	if account >= PathHardenedBase {
		return fmt.Sprintf("m/44'/60'/%d'/0/0", account-PathHardenedBase)
	}
	return fmt.Sprintf("m/44'/60'/%d/0/0", account)
}

func DeriveLedgerAddress(accountIndex uint32) string {
	ledgerhub, err := usbwallet.NewLedgerHub()
	if err != nil {
		log.Println("Unable to open LedgerHub()")
		log.Panic(err)
	}

	wallets := ledgerhub.Wallets()
	if len(wallets) != 1 {
		log.Fatal("Unable to access exactly one wallet")
	}
	wallet := wallets[0]
	wallet.Open("")

	path := DerivationPath(accountIndex)
	account, err := wallet.Derive(path, false)
	if err != nil {
		log.Println("Unable to derive an address")
		log.Panic(err)
	}

	return fmt.Sprintf("0x%s", hex.EncodeToString(account.Address.Bytes()))
}
